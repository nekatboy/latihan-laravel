<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function signup(){
        return view('register');
    }

    public function kirim(Request $request){
        $first_nama = $request['nama1'];
        $last_nama = $request['nama2'];
        return view('welcome2', compact('first_nama','last_nama'));
    }
}
 