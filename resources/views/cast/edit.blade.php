@extends('layout.master')
@section('judul')
Edit Data {{$data->nama}}
@endsection

@section('content')

        <form action="/cast/{{$data->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" value="{{$data->nama}}" name="nama" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" value="{{$data->umur}}" name="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <p><textarea class="form-control" cols="30" rows="10" placeholder="Isi Biodata" name="bio">{{$data->bio}}</textarea></p>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>

@endsection