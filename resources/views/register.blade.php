<html>
    <body>
        <h1>Buat Account Baru !</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post"> 
            @csrf
            <b>
            <p><label for="nama1">First name:</label></p>
            <p><input type="text" placeholder="First name" name="nama1"></p>
            <p><label for="nama2">Last name</label></p>
            <p><input type="text" placeholder="Last name" name="nama2"><br></p>
            <p><label>Gender :</label></p>
            <input type="radio" name="Gender" value="Male" checked>Male <br>
            <input type="radio" name="Gender" value="Female">Female<br>
            <input type="radio" name="Gender" value="Other">Other
            <p><label>Nationality :</label></p>
            <select name="Nationality">
                <optgroup label="ASIA">
                    <option VALUE="Indonesia">Indonesia</option>
                    <option VALUE="Malaysia">Malaysia</option>
                    <option value="Singapura">Singapura</option>
                    <option value="Thailand">Thailand</option>
                    <option value="Jepang">Jepang</option>
                    <option value="China">China</option>
                </optgroup>
                <optgroup label="EROPA">
                    <option VALUE="Jerman">Jerman</option>
                    <option VALUE="Inggris">Inggris</option>
                    <option value="Italia">Italia</option>
                    <option value="Prancis">Prancis</option>
                    <option value="Rusia">Rusia</option>
                    <option value="Turki">Turki</option>
                </optgroup>
            </select>
            <p><label>Language Spoken :</label></p>
            <input type="checkbox" value="Indonesia" name="bahasa" checked>Bahasa Indonesia<br>
            <input type="checkbox" value="English" name="bahasa">English<br>
            <input type="checkbox" value="Other" name="bahasa">Other<br>
            <p><label for="biodata">Bio :</label></p>
            <textarea col="30" rows="10" placeholder="Isi Biodata" id="biodata"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">
            </b>
        </form>
    </body>
</html>