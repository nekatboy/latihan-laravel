<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     return view('index');
// });
Route::get('/', 'IndexController@index');

// Route::get('/register', function () {
//     return view('register');
// });
Route::get('/register', 'AuthController@signup');

// Route::get('/welcome', function () {
//     return view('welcome2');
// });
Route::post('/welcome', 'AuthController@kirim');

Route::get('/datatable', function () {
    return view('layout.tabel.datatable');
});

Route::get('/table', function () {
    return view('layout.tabel.table');
});

//CRUD CAST
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');